﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using nmct.ba.cachlessproject.api.Helper;
using nmct.ba.cashlessproject.model;
using System.Data;
using System.Security.Claims;

namespace nmct.ba.cashlessproject.api.Models
{
    public class RegisterDA
    {
        public static List<Registers> GetRegisters()
        {
            List<Registers> list = new List<Registers>();

            string sql = "SELECT * FROM Registers";
            DbDataReader reader = Database.GetData("ConnectionString", sql);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            return list;
        }

        private static Registers Create(IDataRecord record)
        {
            return new Registers()
            {
                ID = Int32.Parse(record["ID"].ToString()),
                Device = record["Device"].ToString(),
                RegisterName = record["RegisterName"].ToString(),
            };
        }

        public static int InsertRegisters(Registers c, IEnumerable<Claim> claims)
        {
            string sql = "INSERT INTO Registers VALUES(@RegisterName,@Device)";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@RegisterName", c.RegisterName);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@Device", c.Device);
            return Database.InsertData("ConnectionString", sql, par1, par2);
        }

        public static void UpdateRegisters(Registers c, IEnumerable<Claim> claims)
        {
            string sql = "UPDATE Registers SET Device=@Device, RegisterName=@RegisterName WHERE ID=@ID";


            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", c.ID);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@Device", c.Device);
            DbParameter par3 = Database.AddParameter("ConnectionString", "@RegisterName", c.RegisterName);

            Database.ModifyData("ConnectionString", sql, par1, par2, par3);

            /*string sql = "UPDATE Customer SET CustomerName=@CustomerName, Address=@Address, Picture=@Picture, Balance=@Balance WHERE ID=@ID";
            DbParameter par1 = Database.AddParameter("AdminDB", "@CustomerName", c.CustomerName);
            DbParameter par2 = Database.AddParameter("AdminDB", "@Address", c.Address);
            DbParameter par3 = Database.AddParameter("AdminDB", "@Picture", c.Picture);
            DbParameter par4 = Database.AddParameter("AdminDB", "@Balance", c.Balance);
            DbParameter par5 = Database.AddParameter("AdminDB", "@ID", c.ID);
            Database.ModifyData(Database.GetConnection(CreateConnectionString(claims)), sql, par1, par2, par3, par4, par5);*/
        }

        public static void DeleteRegisters(int id, IEnumerable<Claim> claims)
        {
            string sql = "DELETE FROM Registers WHERE ID=@ID";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", id);
            Database.ModifyData("ConnectionString", sql, par1);
        }

        public static Registers GetRegistersByID(int p)
        {
            List<Registers> list = new List<Registers>();

            string sql = "SELECT * FROM Registers where ID = @ID";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", p);
            DbDataReader reader = Database.GetData("ConnectionString", sql, par1);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            return list[0];
        }
    }
}