﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using nmct.ba.cashlessproject.model;
using System.Data.Common;
using nmct.ba.cachlessproject.api.Helper;
using System.Data;

namespace nmct.ba.cachlessproject.api.Models
{
    public class ProductDA
    {
        public static List<Products> GetProducts()
        {
            List<Products> list = new List<Products>();

            string sql = "SELECT * FROM Products";
            DbDataReader reader = Database.GetData("ConnectionString", sql);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            return list;
        }

        public static Products GetProduct(int id)
        {

            List<Products> list = new List<Products>();

            string sql = "SELECT * FROM Products where ID = @ID";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", id);
            DbDataReader reader = Database.GetData("ConnectionString", sql, par1);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            return list[0];
        }
        private static Products Create  (IDataRecord record)
        {
            return new Products()
            {
                ID = Int32.Parse(record["ID"].ToString()),
                ProductName = record["ProductName"].ToString(),
                Price = float.Parse(record["Price"].ToString())
            };
        }
        public static int InsertProduct(Products c, IEnumerable<Claim> claims)
        {
            string sql = "INSERT INTO Products VALUES(@ProductName,@Price)";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@ProductName", c.ProductName);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@Price", c.Price);
            return Database.InsertData("ConnectionString", sql, par1, par2);
        }

        public static void UpdateProducts(Products p, IEnumerable<Claim> claims)
        {
            string sql = "UPDATE Products SET ProductName=@ProductName, Price=@Price WHERE ID=@ID";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@ProductName", p.ProductName);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@Price", p.Price);
            DbParameter par5 = Database.AddParameter("ConnectionString", "@ID", p.ID);
            Database.ModifyData("ConnectionString", sql, par1, par2, par5);
        }
        public static void DeleteProducts(int id, IEnumerable<Claim> claims)
        {
            string sql = "DELETE FROM Products WHERE ID=@ID";
            DbParameter par5 = Database.AddParameter("ConnectionString", "@ID", id);
            Database.ModifyData("ConnectionString", sql, par5);
        }

    }
}