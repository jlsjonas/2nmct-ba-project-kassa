﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using nmct.ba.cashlessproject.model;
using System.Data.Common;
using nmct.ba.cachlessproject.api.Helper;
using System.Data;

namespace nmct.ba.cachlessproject.api.Models
{
    public class EmployeeDA
    {
        public static List<Employee> GetEmployee()
        {
            List<Employee> list = new List<Employee>();

            string sql = "SELECT * FROM Employee";
            DbDataReader reader = Database.GetData("ConnectionString", sql);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            return list;
        }
        private static Employee Create  (IDataRecord record)
        {
            return new Employee()
            {
                ID = Int32.Parse(record["ID"].ToString()),
                EmployeeName = record["EmployeeName"].ToString(),
                Address = record["Address"].ToString(),
                Email = record["Email"].ToString(),
                Phone = record["Phone"].ToString()
            };
        }

        public static int InsertEmployee(Employee c, IEnumerable<Claim> claims)
        {
            string sql = "INSERT INTO Employee VALUES(@EmployeeName,@Address,@Email,@Phone)";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@EmployeeName", c.EmployeeName);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@Address", c.Address);
            DbParameter par3 = Database.AddParameter("ConnectionString", "@Email", c.Email);
            DbParameter par4 = Database.AddParameter("ConnectionString", "@Phone", c.Phone);
            return Database.InsertData("ConnectionString", sql, par1, par2, par3, par4);
        }

        public static void UpdateEmployee(Employee c, IEnumerable<Claim> claims)
        {
            string sql = "UPDATE Employee SET EmployeeName=@EmployeeName, Address=@Address, Email=@Email, Phone=@Phone WHERE ID=@ID";


            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", c.ID);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@EmployeeName", c.EmployeeName);
            DbParameter par3 = Database.AddParameter("ConnectionString", "@Address", c.Address);
            DbParameter par4 = Database.AddParameter("ConnectionString", "@Email", c.Email);
            DbParameter par5 = Database.AddParameter("ConnectionString", "@Phone", c.Phone);

            Database.ModifyData("ConnectionString", sql, par1, par2, par3, par4, par5);
        }
        public static void DeleteEmployee(int id, IEnumerable<Claim> claims)
        {
            string sql = "DELETE FROM Employee WHERE ID=@ID";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", id);
            Database.ModifyData("ConnectionString", sql, par1);
        }

        public static Employee GetEmployeeBySerialNumber(String p)
        {
            List<Employee> list = new List<Employee>();

            string sql = "SELECT * FROM Employee where SerialNumber = @p";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@p", p);
            DbDataReader reader = Database.GetData("ConnectionString", sql, par1);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            if (list.Count > 0)
                return list[0];
            else
                return new Employee();
        }

    }
}