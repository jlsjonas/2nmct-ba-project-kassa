﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using nmct.ba.cashlessproject.api.Models;
using nmct.ba.cashlessproject.model;
using System.Data.Common;
using nmct.ba.cachlessproject.api.Helper;
using System.Data;

namespace nmct.ba.cachlessproject.api.Models
{
    public class SalesDA
    {
        public static List<Sales> GetSales()
        {
            List<Sales> list = new List<Sales>();

            string sql = "SELECT * FROM Sales";
            DbDataReader reader = Database.GetData("ConnectionString", sql);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            return list;
        }
        private static Sales Create  (IDataRecord record)
        {
            return new Sales()
            {
                ID = Int32.Parse(record["ID"].ToString()),

                Product = ProductDA.GetProduct(int.Parse(record["ProductID"].ToString())),
                Customer = CustomerDA.GetCustomerById(int.Parse(record["CustomerID"].ToString())),
                Register = RegisterDA.GetRegistersByID(int.Parse(record["RegisterID"].ToString())),


                Amount = double.Parse(record["Amount"].ToString()),
                

                Timestamp = DateTime.Parse(record["Timestamp"].ToString()),
//                Timestamp = DateTime.Parse(record["Timestamp"].ToString()),
                TotalPrice = float.Parse(record["TotalPrice"].ToString()),
            };
        }
        public static int InsertSale(Sales c, IEnumerable<Claim> claims)
        {
            string sql = "INSERT INTO Sales(Timestamp,CustomerID,RegisterID,ProductID,Amount,TotalPrice)VALUES (@Timestamp,@CustomerID,@RegisterID,@ProductID,@Amount,@TotalPrice)";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@Timestamp", c.Timestamp);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@CustomerID", c.Customer.ID);
            DbParameter par3 = Database.AddParameter("ConnectionString", "@RegisterID", c.Register.ID);
            DbParameter par4 = Database.AddParameter("ConnectionString", "@ProductID", c.Product.ID);
            DbParameter par5 = Database.AddParameter("ConnectionString", "@Amount", c.Amount);
            DbParameter par6 = Database.AddParameter("ConnectionString", "@TotalPrice", c.TotalPrice);
            return Database.InsertData("ConnectionString", sql, par1, par2, par3, par4,par5,par6);
        }
//
//        public static void UpdateSales(Sales p, IEnumerable<Claim> claims)
//        {
//            string sql = "UPDATE Sales SET StatisticName=@StatisticName, Price=@Price WHERE ID=@ID";
//            DbParameter par1 = Database.AddParameter("ConnectionString", "@StatisticName", p.StatisticName);
//            DbParameter par2 = Database.AddParameter("ConnectionString", "@Price", p.Price);
//            DbParameter par5 = Database.AddParameter("ConnectionString", "@ID", p.ID);
//            Database.ModifyData("ConnectionString", sql, par1, par2, par5);
//        }
//        public static void DeleteSales(int id, IEnumerable<Claim> claims)
//        {
//            string sql = "DELETE FROM Sales WHERE ID=@ID";
//            DbParameter par5 = Database.AddParameter("ConnectionString", "@ID", id);
//            Database.ModifyData("ConnectionString", sql, par5);
//        }

    }
}