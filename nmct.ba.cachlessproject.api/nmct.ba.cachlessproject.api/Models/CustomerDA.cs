﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using nmct.ba.cashlessproject.model;
using System.Data.Common;
using nmct.ba.cachlessproject.api.Helper;
using System.Data;

namespace nmct.ba.cachlessproject.api.Models
{
    public class CustomerDA
    {
        public static List<Customers> GetCustomers()
        {
            List<Customers> list = new List<Customers>();

            string sql = "SELECT * FROM Customers";
            DbDataReader reader = Database.GetData("ConnectionString", sql);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            return list;
        }
        public static Customers GetCustomer(int id)
        {
            List<Customers> list = new List<Customers>();

            string sql = "SELECT * FROM Customers WHERE ID = @ID LIMIT 1";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", id);
            DbDataReader reader = Database.GetData("ConnectionString", sql, par1);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            return list[0];
        }
        private static Customers Create  (IDataRecord record)
        {
            return new Customers()
            {
                ID = Int32.Parse(record["ID"].ToString()),
                CustomerName = record["CustomerName"].ToString(),
                Address = record["Address"].ToString(),
                Picture = (record["picture"] as byte[]),
                Balance = float.Parse(record["Balance"].ToString()),
                SerialNumber = record["SerialNumber"].ToString()
            };
        }

        public static int InsertCustomer(Customers c, IEnumerable<Claim> claims)
        {
            string sql = "INSERT INTO Customers VALUES(@CustomerName,@Address,@Picture,@Balance,@SN)";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@CustomerName", c.CustomerName);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@Address", c.Address);
            DbParameter par3 = Database.AddParameter("ConnectionString", "@Picture", c.Picture);
            DbParameter par4 = Database.AddParameter("ConnectionString", "@Balance", c.Balance);
            DbParameter par5 = Database.AddParameter("ConnectionString", "@SN", c.SerialNumber);
            return Database.InsertData("ConnectionString", sql, par1, par2, par3, par4, par5);
        }

        public static void UpdateCustomer(Customers c, IEnumerable<Claim> claims)
        {
            string sql = "UPDATE Customers SET CustomerName=@CustomerName, Address=@Address, Picture=@Picture, Balance=@Balance WHERE ID=@ID";


            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", c.ID);
            DbParameter par2 = Database.AddParameter("ConnectionString", "@CustomerName", c.CustomerName);
            DbParameter par3 = Database.AddParameter("ConnectionString", "@Address", c.Address);
            DbParameter par4 = Database.AddParameter("ConnectionString", "@Picture", c.Picture);
            DbParameter par5 = Database.AddParameter("ConnectionString", "@Balance", c.Balance);

            Database.ModifyData("ConnectionString", sql, par1, par2, par3, par4, par5);
        }
        public static void DeleteCustomer(int id, IEnumerable<Claim> claims)
        {
            string sql = "DELETE FROM Customers WHERE ID=@ID";
            DbParameter par5 = Database.AddParameter("ConnectionString", "@ID", id);
            Database.ModifyData("ConnectionString", sql, par5);
        }


        public static Customers GetCustomerById(int p)
        {
            List<Customers> list = new List<Customers>();

            string sql = "SELECT * FROM Customers where ID = @ID";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@ID", p);
            DbDataReader reader = Database.GetData("ConnectionString", sql, par1);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            if(list.Count>0)
                return list[0];
            else
                return new Customers();
        }

        public static Customers GetCustomerBySerialNumber(String p)
        {
            List<Customers> list = new List<Customers>();

            string sql = "SELECT * FROM Customers where SerialNumber = @p";
            DbParameter par1 = Database.AddParameter("ConnectionString", "@p", p);
            DbDataReader reader = Database.GetData("ConnectionString", sql, par1);

            while (reader.Read())
            {
                list.Add(Create(reader));
            }
            reader.Close();
            if(list.Count>0)
                return list[0];
            else
                return new Customers();
        }
    }
}