﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using nmct.ba.cachlessproject.api.Models;
using nmct.ba.cashlessproject.model;

namespace nmct.ba.cachlessproject.api.Controllers
{
    public class EmployeeController : ApiController
    {
        // GET api/<controller>
        public List<Employee> Get()
        {
            return EmployeeDA.GetEmployee();
        }

        // GET api/<controller>/ID
        public Employee Get(String id)
        {
            return EmployeeDA.GetEmployeeBySerialNumber(id);
        }

//        // GET api/<controller>/5
//        public string Get(int id)
//        {
//            return "value";
//        }

        // POST api/<controller>
        public HttpResponseMessage Post(Employee c)
        {
            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
            int id = EmployeeDA.InsertEmployee(c, p.Claims);

            HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.OK);
            message.Content = new StringContent(id.ToString());
            return message;
        }

        // PUT api/<controller>/5
        public HttpResponseMessage Put(Employee c)
        {
            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
            EmployeeDA.UpdateEmployee(c, p.Claims);

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
            EmployeeDA.DeleteEmployee(id, p.Claims);
        }
    }
}