﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using nmct.ba.cachlessproject.api.Models;
using nmct.ba.cashlessproject.api.Models;
using nmct.ba.cashlessproject.model;

namespace nmct.ba.cachlessproject.api.Controllers
{
    public class SalesController : ApiController
    {
        // GET: api/Register
        public List<Sales> Get()
        {
            return SalesDA.GetSales();
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public HttpResponseMessage Post(Sales c)
        {
            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
            int id = SalesDA.InsertSale(c, p.Claims);

            HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.OK);
            message.Content = new StringContent(id.ToString());
            return message;
        }

//        // PUT api/<controller>/5
//        public HttpResponseMessage Put(Sales c)
//        {
//            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
//            SalesDA.UpdateSales(c, p.Claims);
//
//            return new HttpResponseMessage(HttpStatusCode.OK);
//        }
//
//        // DELETE api/<controller>/5
//        public void Delete(int id)
//        {
//            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
//            SalesDA.DeleteSales(id, p.Claims);
//        }
    }
}
