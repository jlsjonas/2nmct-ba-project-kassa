﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using nmct.ba.cachlessproject.api.Models;
using nmct.ba.cashlessproject.model;

namespace nmct.ba.cachlessproject.api.Controllers
{
    public class CustomerController : ApiController
    {
        // GET api/<controller>
        public List<Customers> Get()
        {
            return CustomerDA.GetCustomers();
        }
        // GET api/<controller>/ID
        public Customers Get(String id)
        {
            return CustomerDA.GetCustomerBySerialNumber(id);
        }


        // POST api/<controller>
        public HttpResponseMessage Post(Customers c)
        {
            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
            int id = CustomerDA.InsertCustomer(c, p.Claims);

            HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.OK);
            message.Content = new StringContent(id.ToString());
            return message;
        }

        // PUT api/<controller>/5
        public HttpResponseMessage Put(Customers c)
        {
            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
            CustomerDA.UpdateCustomer(c, p.Claims);

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            ClaimsPrincipal p = RequestContext.Principal as ClaimsPrincipal;
            CustomerDA.DeleteCustomer(id, p.Claims);
        }
    }
}