﻿using System.IO;
using System.Net.Http;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmct.ba.cashlessproject.ui.B.ViewModel
{
    class customersVM : ObservableObject, IPage
    {
        private ObservableCollection<Customers> _customers;

        public ObservableCollection<Customers> Customers
        {
            get { return _customers; }
            set { _customers = value; OnPropertyChanged("Customers"); }
        }
        public customersVM()
        {
           
                GetCustomers();
            
        }

        private async void GetCustomers()
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Customer");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Customers = JsonConvert.DeserializeObject<ObservableCollection<Customers>>(json);
                }
            }
        }
        public string Name
        {
            get { return "Klanten"; }
        }

        private Customers _selected;
        public Customers SelectedCustomer
        {
            get { return _selected; }
            set { _selected = value; OnPropertyChanged("SelectedCustomer"); }
        }

        //public event PropertyChangedEventHandler PropertyChanged;

        //void OnPropertyChanged(string propertyName)
        //{
        //    if (PropertyChanged != null)
        //        PropertyChanged(this, new PropertyChangedEventArgs("Customers"));
        //}

        public ICommand NewCustomerCommand
        {
            get { return new RelayCommand(NewCustomer); }
        }
        private void NewCustomer()
        {
            var c = new Customers();
            Customers.Add(c);
            SelectedCustomer = c;
        }

        public ICommand DeleteCustomerCommand
        {
            get { return new RelayCommand(DeleteCustomer); }
        }

        private async void DeleteCustomer()
        {
            using (HttpClient client = new HttpClient())
            {
                //client.SetBearerToken(ApplicationVM.token.AccessToken);
                HttpResponseMessage response = await client.DeleteAsync("http://localhost:36795/api/Customer/" + SelectedCustomer.ID);
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine("error");
                }
                else
                {
                    Customers.Remove(SelectedCustomer);
                }
            }
        }

        public ICommand SaveCustomerCommand
        {
            get { return new RelayCommand<int>(SaveCustomer);}
        }

        public async void SaveCustomer(int ID)
        {
            //            int ID = 0;
            string input = JsonConvert.SerializeObject(SelectedCustomer);

            // check insert (no ID assigned) or update (already an ID assigned)
            if (ID == 0)
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PostAsync("http://localhost:36795/api/Customer", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string output = await response.Content.ReadAsStringAsync();
                        ID = Int32.Parse(output);
                    }
                    else
                    {
                        Console.WriteLine("error");
                    }
                }
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PutAsync("http://localhost:36795/api/Customer", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("error");
                    }
                }
            }
        }
        public ICommand AddImageCommand
        {
            get { return new RelayCommand(AddImage); }
        }

        private void AddImage()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                SelectedCustomer.Picture = GetPhoto(ofd.FileName);
                OnPropertyChanged("SelectedCustomer");
            }
            SaveCustomer(SelectedCustomer.ID);
        }


//        public byte[] imageToByteArray(System.Drawing.Image imageIn)
//        {
//            MemoryStream ms = new MemoryStream();
//            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
//            return ms.ToArray();
//        }

        private byte[] GetPhoto(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] data = new byte[fs.Length];
            fs.Read(data, 0, (int)fs.Length);
            fs.Close();

            return data;
        }
    }
}
