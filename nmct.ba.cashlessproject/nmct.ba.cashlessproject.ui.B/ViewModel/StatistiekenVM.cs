﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;
using nmct.ba.cashlessproject.ui.B.Views;

namespace nmct.ba.cashlessproject.ui.B.ViewModel
{
    class StatistiekenVM : ObservableObject, IPage
    {
        public string Name
        {
            get { return "Statistieken";  }
        }
    
        private ObservableCollection<Sales> _statistieken;

        public ObservableCollection<Sales> Statistieken
        {
            get { return _statistieken; }
            set { _statistieken = value; OnPropertyChanged("Statistieken"); }
        }

        public StatistiekenVM()
        {
           
                GetStatistieken();
                GetRegisters();
                GetCustomers();
                GetProducts();

        }

        private async void GetStatistieken()
        {
            using (HttpClient client = new HttpClient())
            {
                try {
                    HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Sales");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Statistieken = JsonConvert.DeserializeObject<ObservableCollection<Sales>>(json);
                }
                    }
                catch (Exception e)
                {
                    //TODO: catch e
                }
                
            }
        }

//        public ObservableCollection<Customers> GetCustomers
//        {
//            get
//            {
//
//                Customers = _customersVM.Customers;
//                return Customers;
//            }
//        }
        private ObservableCollection<Customers> _customers;
        public ObservableCollection<Customers> Customers
        {
            get
            {
                //                _customers = _customersVM.Customers;
                return _customers;
            }
            set { _customers = value; OnPropertyChanged("Customers"); }
        }
        private async void GetCustomers()
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Customer");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Customers = JsonConvert.DeserializeObject<ObservableCollection<Customers>>(json);
                }
            }
        }private ObservableCollection<model.Registers> _Registers;
        public ObservableCollection<model.Registers> Registers
        {
            get
            {
                //                _Registers = _RegistersVM.Registers;
                return _Registers;
            }
            set { _Registers = value; OnPropertyChanged("Registers"); }
        }
        private async void GetRegisters()
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Register");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Registers = JsonConvert.DeserializeObject<ObservableCollection<model.Registers>>(json);
                }
            }
        }private ObservableCollection<Products> _Products;
        public ObservableCollection<Products> Products
        {
            get
            {
                //                _Products = _ProductsVM.Products;
                return _Products;
            }
            set { _Products = value; OnPropertyChanged("Products"); }
        }
        private async void GetProducts()
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Product");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Products = JsonConvert.DeserializeObject<ObservableCollection<Products>>(json);
                }
            }
        }

//        static ObservableCollection<model.Registers> GetRegisters()
//        {
//            var cVM = new RegisterVM();
//
//            return cVM.Registers;
//        }
//
//        static ObservableCollection<Products> GetProducts()
//        {
//            var cVM = new ProductsVM();
//
//            return cVM.Products;
//        }

        private Sales _selected;
        public Sales SelectedSale
        {
            get { return _selected; }
            set { _selected = value; OnPropertyChanged("SelectedSale"); }
        }
        private Sales _selectedRegister;
        public Sales SelectedRegister
        {
            get { return _selectedRegister; }
            set { _selectedRegister = value; OnPropertyChanged("SelectedRegister"); }
        }
        private Sales _selectedProduct;
        public Sales SelectedProduct
        {
            get { return _selectedProduct; }
            set { _selectedProduct = value; OnPropertyChanged("SelectedProduct"); }
        }
    }
}