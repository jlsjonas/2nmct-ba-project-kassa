﻿using System.ComponentModel;
using System.Net.Http;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmct.ba.cashlessproject.ui.B.ViewModel
{
    class ProductsVM : ObservableObject, IPage
    {
        private ObservableCollection<Products> _products;

        public ObservableCollection<Products> Products
        {
            get { return _products; }
            set
            {
                _products = value; OnPropertyChanged("Products");
            }
        }
        public ProductsVM()
        {
           
                GetProducts();
            
        }

        private async void GetProducts()
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Product");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Products = JsonConvert.DeserializeObject<ObservableCollection<Products>>(json);
                }
            }
        }
        public string Name
        {
            get { return "products"; }
        }

        private Products _selected;
        public Products SelectedProduct
        {
            get { return _selected; }
            set { _selected = value; OnPropertyChanged("SelectedProduct"); }
        }

        //public event PropertyChangedEventHandler PropertyChanged;

        //void OnPropertyChanged(string propertyName)
        //{
        //    if (PropertyChanged != null)
        //        PropertyChanged(this, new PropertyChangedEventArgs("Products"));
        //}

        public ICommand NewProductCommand
        {
            get { return new RelayCommand(NewProduct); }
        }
        private void NewProduct()
        {
            var c = new Products();
            Products.Add(c);
            SelectedProduct = c;
        }

        public ICommand DeleteProductCommand
        {
            get { return new RelayCommand(DeleteProduct); }
        }

        private async void DeleteProduct()
        {
            using (HttpClient client = new HttpClient())
            {
                //client.SetBearerToken(ApplicationVM.token.AccessToken);
                HttpResponseMessage response = await client.DeleteAsync("http://localhost:36795/api/Product/" + SelectedProduct.ID);
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine("error");
                }
                else
                {
                    Products.Remove(SelectedProduct);
                }
            }
        }

        public ICommand SaveProductCommand
        {
            get { return new RelayCommand<DataGridRowEditEndingEventArgs>(SaveProduct); }
        }

        public async void SaveProduct(DataGridRowEditEndingEventArgs e)
        {
            SelectedProduct = e.Row.Item as Products;
            int ID = SelectedProduct.ID;
//            int ID = 0;
            string input = JsonConvert.SerializeObject(SelectedProduct);
            
            // check insert (no ID assigned) or update (already an ID assigned)
            if (ID == 0)
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PostAsync("http://localhost:36795/api/Product", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string output = await response.Content.ReadAsStringAsync();
                        ID = Int32.Parse(output);
                        SelectedProduct.ID=ID;
//                        Products = Products;
                    }
                    else
                    {
                        Console.WriteLine("error");
                    }
                }
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PutAsync("http://localhost:36795/api/Product", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("error");
                    }
                }
            }
        }
    }
}
