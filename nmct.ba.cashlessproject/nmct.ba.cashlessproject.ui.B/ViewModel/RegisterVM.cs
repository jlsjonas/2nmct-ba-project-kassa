﻿using System.Net.Http;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmct.ba.cashlessproject.ui.B.ViewModel
{
    class RegisterVM : ObservableObject, IPage
    {
        private ObservableCollection<Registers> _registers;

        public ObservableCollection<Registers> Registers
        {
            get { return _registers; }
            set { _registers = value; OnPropertyChanged("Registers"); }
        }
        public RegisterVM()
        {
           
                GetRegisters();
            
        }

        private async void GetRegisters()
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Register");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Registers = JsonConvert.DeserializeObject<ObservableCollection<Registers>>(json);
                }
            }
        }
        public string Name
        {
            get { return "Kassa's"; }
        }
    }
}
