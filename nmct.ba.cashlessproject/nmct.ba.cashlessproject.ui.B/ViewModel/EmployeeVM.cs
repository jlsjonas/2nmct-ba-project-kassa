﻿using System.Net.Http;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmct.ba.cashlessproject.ui.B.ViewModel
{
    class EmployeeVM : ObservableObject, IPage
    {
        private ObservableCollection<Employee> _Employee;

        public ObservableCollection<Employee> Employee
        {
            get { return _Employee; }
            set { _Employee = value; OnPropertyChanged("Employee"); }
        }
        public EmployeeVM()
        {
           
                GetEmployees();
            
        }


        private async void GetEmployees()
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Employee");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Employee = JsonConvert.DeserializeObject<ObservableCollection<Employee>>(json);
                }
            }
        }

        private Employee _selected;
        public Employee SelectedEmployee
        {
            get { return _selected; }
            set { _selected = value; OnPropertyChanged("SelectedEmployee"); }
        }

        //public ICommand NewCustomerCommand
        //{
        //    get { return new RelayCommand(NewCustomer); }
        //}

        //public ICommand SaveCustomerCommand
        //{
        //    get { return new RelayCommand(SaveCustomer); }
        //}

        //public ICommand DeleteCustomerCommand
        //{
        //    get { return new RelayCommand(DeleteCustomer); }
        //}
        public string Name
        {
            get { return "Employees"; }
        }


        public ICommand NewEmployeeCommand
        {
            get { return new RelayCommand(NewEmployee); }
        }
        private void NewEmployee()
        {
            var c = new Employee();
            Employee.Add(c);
            SelectedEmployee = c;
        }

        public ICommand DeleteEmployeeCommand
        {
            get { return new RelayCommand(DeleteEmployee); }
        }

        private async void DeleteEmployee()
        {
            using (HttpClient client = new HttpClient())
            {
                //client.SetBearerToken(ApplicationVM.token.AccessToken);
                HttpResponseMessage response = await client.DeleteAsync("http://localhost:36795/api/Employee/" + SelectedEmployee.ID);
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine("error");
                }
                else
                {
                    Employee.Remove(SelectedEmployee);
                }
            }
        }

        public ICommand SaveEmployeeCommand
        {
            get { return new RelayCommand<int>(SaveEmployee); }
        }

        public async void SaveEmployee(int ID)
        {
            //            int ID = 0;
            string input = JsonConvert.SerializeObject(SelectedEmployee);

            // check insert (no ID assigned) or update (already an ID assigned)
            if (ID == 0)
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PostAsync("http://localhost:36795/api/Employee", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string output = await response.Content.ReadAsStringAsync();
                        ID = Int32.Parse(output);
                    }
                    else
                    {
                        Console.WriteLine("error");
                    }
                }
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PutAsync("http://localhost:36795/api/Employee", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("error");
                    }
                }
            }
        }
    }
}
