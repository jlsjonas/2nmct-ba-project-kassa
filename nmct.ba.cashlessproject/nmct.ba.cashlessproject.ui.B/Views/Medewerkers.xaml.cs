﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using nmct.ba.cashlessproject.model;
using nmct.ba.cashlessproject.ui.B.ViewModel;

namespace nmct.ba.cashlessproject.ui.B.Views
{
	/// <summary>
	/// Interaction logic for Producten.xaml
	/// </summary>
	public partial class Medewerkers : UserControl
	{
		public Medewerkers()
		{
			this.InitializeComponent();
		}

        private void dGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (this.dGrid.SelectedItem != null)
            {
                (sender as DataGrid).RowEditEnding -= dGrid_RowEditEnding;
                (sender as DataGrid).CommitEdit();
                EmployeeVM pvm = new EmployeeVM();
                pvm.SelectedEmployee = e.Row.Item as Employee;
                pvm.SaveEmployee(pvm.SelectedEmployee.ID);
                (sender as DataGrid).Items.Refresh();
                (sender as DataGrid).RowEditEnding += dGrid_RowEditEnding;
                (sender as DataGrid).Items.Refresh();

            }
            else return;
        }
	}
}