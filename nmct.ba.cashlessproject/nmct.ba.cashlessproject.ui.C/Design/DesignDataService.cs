﻿using System;
using nmct.ba.cashlessproject.ui.C.Model;

namespace nmct.ba.cashlessproject.ui.C.Design
{
    public class DesignDataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to create design time data

            var item = new DataItem("Welcome to MVVM Light [design]");
            callback(item, null);
        }
    }
}