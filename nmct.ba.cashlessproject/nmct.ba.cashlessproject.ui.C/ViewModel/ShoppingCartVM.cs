﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;
using nmct.ba.cashlessproject.ui.C.Model;

namespace nmct.ba.cashlessproject.ui.C.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    class ShoppingCartVM : ObservableObject, IPage
    {
        private Customers _customer;
        private readonly Employee _employee;
        private float _totalOrder;
        private float _calculationOrder;
        private ObservableCollection<Products> _products;
        private ObservableCollection<Sales> _newSales; 

        public ShoppingCartVM(Customers customers, Employee employee)
        {
            Customer = customers;
            _employee = employee;
            CalculationOrder = Customer.Balance;
            NewSales = new ObservableCollection<Sales>();
            TotalOrder = 0;

            GetProducts();
        }
        public ShoppingCartVM()
        {
            TotalOrder = 0;
            CalculationOrder = 0;
//            Customers = customers;
//            _employee = employee;
            GetProducts();
        }

//        publ
        public string Name
        {
            get { return "ShoppingCart"; }
        }

        public Employee Employee
        {
            get { return _employee; }
        }

        public Customers Customer
        {
            get { return _customer; }
            set
            {
                _customer = value;
                OnPropertyChanged("Customer");
            }
        }


        public ObservableCollection<Products> Products
        {

            get { return _products; }
            set { _products = value; OnPropertyChanged("Products"); }
        }
        private async void GetProducts()
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Product");
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Products = JsonConvert.DeserializeObject<ObservableCollection<Products>>(json);
                }
            }
        }
        public ObservableCollection<Sales> NewSales
        {
            get { return _newSales; }
            set { _newSales = value; }
        }


        public float TotalOrder
        {
            get { return _totalOrder; }
            set
            {
                _totalOrder = value;
                OnPropertyChanged("TotalOrder");
            }
        }

        public float CalculationOrder
        {
            get { return _calculationOrder; }
            set
            {
                _calculationOrder = value;
                OnPropertyChanged("CalculationOrder");
            }
        }
        public ICommand AddProductCommand
        {
            get { return new RelayCommand<Products>(AddSelectedProduct); }
        }
        public ICommand SaveSaleCommand
        {
            get { return new RelayCommand(SaveSaleCollection); }
        }

        public async void SaveSale(Sales Sale)
        {
            string input = JsonConvert.SerializeObject(Sale);

            // check insert (no ID assigned) or update (already an ID assigned)
            if (Sale.ID == 0)
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PostAsync("http://localhost:36795/api/Sales", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string output = await response.Content.ReadAsStringAsync();
                        Sale.ID = Int32.Parse(output);
                    }
                    else
                    {
                        Console.WriteLine("error");
                    }
                }
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PutAsync("http://localhost:36795/api/Sales", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("error");
                    }
                    else
                    {
                        //var q = Products.IndexOf(Products.Where(X => X.ID == SelectedProduct.ID).FirstOrDefault());
                        //Products[q].ID = SelectedProduct.ID;
                    }
                }
            }
        }

        public async void SaveCustomer()
        {
            //            int ID = 0;
            string input = JsonConvert.SerializeObject(Customer);

            // check insert (no ID assigned) or update (already an ID assigned)
            if (Customer.ID != 0)
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PutAsync("http://localhost:36795/api/Customer", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("error");
                    }
                }
            }
        }

        public void SaveSaleCollection()
        {
            //save customer balance
            Customer.Balance = CalculationOrder;
            SaveCustomer();

            //TODO save sale to db/api
            var currentDateTime = DateTime.Now;
            foreach (var sale in NewSales)
            {
                sale.Timestamp = currentDateTime;
                SaveSale(sale);
            }
            (App.Current.MainWindow.DataContext as ApplicationVM).ChangePage(new LoginCustomersVM(Employee));
        }

        public ObservableCollection<Sales> Sales
        {
            get { return _newSales; }
            set { _newSales = value; }
        }

        private void AddSelectedProduct(Products b)
        {
            CalculationOrder -= b.Price;
            if (CalculationOrder >= 0)
            {
                bool inFor = false;
//                var temp2 = NewSales.Where(x => x.Product == b);
                for (int i = 0; i < NewSales.Count; i++)
                {
                    if (b == NewSales[i].Product)
                    {
                        inFor = true;
                        NewSales[i].Amount++;
                        NewSales[i].TotalPrice = NewSales[i].Amount*b.Price;
                    }
                }
                if (!inFor)
                {
                    var temp = new Sales();
                    temp.Product = b;
                    temp.Customer = Customer;
                    temp.Amount = 1;
                    temp.TotalPrice = b.Price;
                    temp.Register = new Registers(){ID = 1};
                    NewSales.Add(temp);

                }
                TotalOrder += b.Price;
            }
            else
                CalculationOrder += b.Price;

        }
    }
}