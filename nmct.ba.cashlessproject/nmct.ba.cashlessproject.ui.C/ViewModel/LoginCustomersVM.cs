﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Egelke.Eid.Client;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;

namespace nmct.ba.cashlessproject.ui.C.ViewModel
{
    class LoginCustomersVM : ObservableObject, IPage
    {

        private Thread test;
        public string Name
        {
            get { return "Login"; }
        }

        private readonly Employee _employee;

        public LoginCustomersVM()
        {
            
        }

        public LoginCustomersVM(Employee employee)
        {
            _employee = employee;
            //TODO: readEid keeps blocking UI (bullshit), handling trough VM again (untill code-behind in view is able to load it's view when requested)
//			readEid();
//            test = new Thread(new ThreadStart(readEid));
//            test.Start();
        }

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(readEid);
            }
        }

        public void readEid()
        {
            // Insert code required on object creation below this point.
            using (Readers readers = new Readers(ReaderScope.User))
            {
                readers.EidCardRequest += readers_EidCardRequest;
                readers.EidCardRequestCancellation += readers_EidCardRequestCancellation;
                EidCard target = readers.WaitForEid(new TimeSpan(0, 0, 10));
                using (target)
                {
                    X509Certificate2 auth = target.ReadCertificate(CertificateId.Authentication);
                    var s = auth.Subject.Split(',');
                    //                    var final = new Array[s.Length];
                    IDictionary<string, string> final = new Dictionary<string, string>();
                    foreach (String s1 in s)
                    {
                        String temp = s1.StartsWith(" ") ? s1.Substring(1) : s1;
                        String[] tarr = temp.Split('=');
                        final[tarr[0]] = tarr[1];
                    }
                    var tc = new Customers() { CustomerName = final["G"] + " " + final["SN"], SerialNumber = final["SERIALNUMBER"] };
                    Console.WriteLine(final["SERIALNUMBER"]);
                    GetCustomerByID(tc);
                }
            }
        }
        private void readers_EidCardRequest(object sender, EventArgs e)
        {
            System.Console.WriteLine("Please insert eID Card");
            // Force WPF to render UI changes immediately with this magic line of code...
            //Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, (Action)delegate() { });
            Application.Current.Dispatcher.Invoke(new Action(() => { }), DispatcherPriority.ContextIdle);

        }

        private void readers_EidCardRequestCancellation(object sender, EventArgs e)
        {
            System.Console.WriteLine("eID Card inserted");
            //TODO handle EID card
            System.Console.WriteLine(sender);
            System.Console.WriteLine(e);
        }

        private Customers _customer;

        public Customers Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public Employee Employee
        {
            get { return _employee; }
        }

        private void GetCustomerByID(Customers tc)
        {
            GetCustomerByID(tc.SerialNumber, tc);
        }
        private async void GetCustomerByID(string id, Customers tc)
        {
            using (HttpClient client = new HttpClient())
            {

                HttpResponseMessage response = await client.GetAsync("http://localhost:36795/api/Customer/"+id);
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    Customer = JsonConvert.DeserializeObject<Customers>(json);
                    if (Customer.ID == 0)
                    {
                        Customer.Picture = new byte[0];
                        Customer.Address = "";

                        string input = JsonConvert.SerializeObject(tc);
                        //TODO: handle new customer UI
                        HttpResponseMessage response2 =
                            await
                                client.PostAsync("http://localhost:36795/api/Customer",
                                    new StringContent(input, Encoding.UTF8, "application/json"));
                        if (response2.IsSuccessStatusCode)
                        {
                            string output = await response2.Content.ReadAsStringAsync();
                            tc.ID = Int32.Parse(output);
                        }
                        else
                        {
                            Console.WriteLine("error");
                        }
                    }
                    else
                    {
                        //TODO: handle existing user using multithreading
//                        ApplicationVM appvm = App.Current.MainWindow.DataContext as ApplicationVM;
//                        //IngelogdklantVM vv = new IngelogdklantVM();
//                        //vv.Klanten = Klanten;
//                        appvm.ChangePage(new LoggedInCustomerVM(Customer));
                        Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate
                        {
//                            ApplicationVM appvm = 
                                (App.Current.MainWindow.DataContext as ApplicationVM).ChangePage(new ShoppingCartVM(Customer,Employee));
//                            appvm.ChangePage(new LoggedInCustomerVM(Customer));
//                            test.abort();
                        }));
                    }
                }
            }
        }
    }
}
