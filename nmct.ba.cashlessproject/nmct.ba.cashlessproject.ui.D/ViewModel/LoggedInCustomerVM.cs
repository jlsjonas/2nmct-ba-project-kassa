﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;

namespace nmct.ba.cashlessproject.ui.D.ViewModel
{
    class LoggedInCustomerVM : ObservableObject, IPage
    {
        private Customers _customers;

        public Customers Customers
        {
            get { return _customers; }
            set { _customers = value; OnPropertyChanged("Customers"); }
        }
        public LoggedInCustomerVM()
        {
        }
        public LoggedInCustomerVM(Customers c)
        {
            Customers = c;
        }
        public string Name
        {
            get { return "Login"; }
        }
        public ICommand AddEurCommand
        {
            get { return new RelayCommand<string>(AddBalance); }
        }
        public ICommand CloseCommand
        {
            get
            {
                return new RelayCommand(CloseBalance);
            }
        }

        private void CloseBalance()
        {
            //TODO: return to login / close customer

            ApplicationVM appvm = App.Current.MainWindow.DataContext as ApplicationVM;
            //IngelogdklantVM vv = new IngelogdklantVM();
            //vv.Klanten = Klanten;
            appvm.ChangePage(new LoginRegisterVM());
        }

        private void AddBalance(string s)
        {
            //TODO: handle balance modifications
            Customers.Balance += int.Parse(s);
            SaveCustomer();
        }

        public async void SaveCustomer()
        {
            string input = JsonConvert.SerializeObject(Customers);

            // check insert (no ID assigned) or update (already an ID assigned)
            if (Customers.ID == 0)
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response =
                        await
                            client.PostAsync("http://localhost:2682/api/Customer",
                                new StringContent(input, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string output = await response.Content.ReadAsStringAsync();
                        Customers.ID = Int32.Parse(output);
                    }
                    else
                    {
                        Console.WriteLine("error");
                    }
                }
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response =
                        await
                            client.PutAsync("http://localhost:36795/api/Customer",
                                new StringContent(input, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("error");
                    }
                }
            }
        }
    }
}
