﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using Newtonsoft.Json;
using nmct.ba.cashlessproject.model;

namespace nmct.ba.cashlessproject.ui.D.ViewModel
{
    class RegisterVM : ObservableObject
    {
        public RegisterVM()
        {
            SelectedCustomer = new Customers();
        }

        private Customers _selected;
        public Customers SelectedCustomer
        {
            get { return _selected; }
            set { _selected = value; OnPropertyChanged("SelectedCustomer"); }
        }

        private void UpdateSelectedName()
        {
            SelectedCustomer.CustomerName = _name + " " + _firstName;
        }

        private String _name;
        public String NameCustomer
        {
            get { return _name; }
            set
            {
                _name = value; OnPropertyChanged("NameCustomer");
                UpdateSelectedName();
            }
        }
        private String _firstName;
        public String FirstNameCustomer
        {
            get { return _firstName; }
            set
            {
                _firstName = value; OnPropertyChanged("FirstNameCustomer");
                UpdateSelectedName();
            }
        }

        private void UpdateSelectedAddress()
        {
            SelectedCustomer.Address = _address1 + ", " + _postalcode + " " + _city;
        }

        private String _address1;
        public String Address1Customer
        {
            get { return _address1; }
            set
            {
                _address1 = value; OnPropertyChanged("Address1Customer");
                UpdateSelectedAddress();
            }
        }
        private String _city;
        public String CityCustomer
        {
            get { return _city; }
            set
            {
                _city = value; OnPropertyChanged("CityCustomer");
                UpdateSelectedAddress();
            }
        }
        private String _postalcode;
        public String postalcodeCustomer
        {
            get { return _postalcode; }
            set
            {
                _postalcode = value; OnPropertyChanged("PostalcodeCustomer");
                UpdateSelectedAddress();
            }
        }


        public ICommand SaveCustomerCommand
        {
            get { return new RelayCommand<int>(SaveCustomer); }
        }

        public async void SaveCustomer(int ID)
        {
            //            int ID = 0;

            SelectedCustomer.Picture = new byte[0];
            string input = JsonConvert.SerializeObject(SelectedCustomer);

            // check insert (no ID assigned) or update (already an ID assigned)
            if (ID == 0)
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PostAsync("http://localhost:36795/api/Customer", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string output = await response.Content.ReadAsStringAsync();
                        ID = Int32.Parse(output);
                    }
                    else
                    {
                        Console.WriteLine("error");
                    }
                }
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    //client.SetBearerToken(ApplicationVM.token.AccessToken);
                    HttpResponseMessage response = await client.PutAsync("http://localhost:36795/api/Customer", new StringContent(input, Encoding.UTF8, "application/json"));
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("error");
                    }
                }
            }
        }

    }
}
