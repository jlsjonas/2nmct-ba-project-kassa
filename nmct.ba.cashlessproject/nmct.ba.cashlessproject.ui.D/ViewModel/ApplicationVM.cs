﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace nmct.ba.cashlessproject.ui.D.ViewModel
{
    class ApplicationVM : ObservableObject
    {
//        public static TokenResponse token = null;
        public ApplicationVM()
        {
            //Pages.Add(new ProductsVM());

            //Pages.Add(new PartThreeVM());
            CurrentPage = new LoginRegisterVM();
            //CurrentPage = Pages[0];

        }

        private object _currentpage;
        public object CurrentPage
        {
            get { return _currentpage; }
            set { _currentpage = value; OnPropertyChanged("CurrentPage"); }
        }

        private List<IPage> _pages;
        public List<IPage> Pages
        {
            get
            {
                if (_pages == null)
                    _pages = new List<IPage>();
                return _pages;
            }
        }

        public ICommand ChangePageCommand
        {
            get { return new RelayCommand<IPage>(ChangePage); }
        }

        public void ChangePage(IPage page)
        {
            CurrentPage = page;
        }
    }
}
