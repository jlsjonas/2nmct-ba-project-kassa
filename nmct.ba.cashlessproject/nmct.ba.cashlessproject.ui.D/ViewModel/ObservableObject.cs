﻿using System.ComponentModel;

namespace nmct.ba.cashlessproject.ui.D.ViewModel
{
    class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        //private Action _action;
        //public RelayCommand(Action a)
        //{_action = a;}
        //public bool CanExecute(object parameter)
        //{
        //return true;
        //}
        //public event EventHandler CanExecuteChanged;
        //public void Execute(object parameter){_action();}
    }


}
