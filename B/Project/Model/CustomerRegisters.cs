﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    class CustomerRegisters
    {
        private int _ID;
        private string _registerName;
        private string _device;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string RegisterName
        {
            get { return _registerName; }
            set { _registerName = value; }
        }

        public string Device
        {
            get { return _device; }
            set { _device = value; }
        }
    }
}
