﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    class Products
    {
        private int _ID;
        private string _productName;
        private float _price;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }

        public float Price
        {
            get { return _price; }
            set { _price = value; }
        }
    }
}
