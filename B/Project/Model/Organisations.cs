﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    class Organisations
    {

        public int ID
        {
            get { return this._ID; }
            set { this._ID = value; }
        }

        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string DbName
        {
            get { return _dbName; }
            set { _dbName = value; }
        }

        public string DbLogin
        {
            get { return _dbLogin; }
            set { _dbLogin = value; }
        }

        public string DbPassword
        {
            get { return _dbPassword; }
            set { _dbPassword = value; }
        }

        public string OrganisationName
        {
            get { return _organisationName; }
            set { _organisationName = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        private int _ID;
        private string _login;
        private string _password;
        private string _dbName;
        private string _dbLogin;
        private string _dbPassword;
        private string _organisationName;
        private string _address;
        private string _email;
        private string _phone;
    }
}