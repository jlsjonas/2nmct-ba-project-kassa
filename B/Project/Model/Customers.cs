﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Project.Model
{
    class Customers
    {
        private int _ID;
        private string _customerName;
        private string _address;
        private BitmapImage _picture;
        private float _balance;

        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public BitmapImage Picture
        {
            get { return _picture; }
            set { _picture = value; }
        }

        public float Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }
    }
}
