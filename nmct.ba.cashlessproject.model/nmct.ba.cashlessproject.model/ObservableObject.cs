﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmct.ba.cashlessproject.model
{
    public class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        //private Action _action;
        //public RelayCommand(Action a)
        //{_action = a;}
        //public bool CanExecute(object parameter)
        //{
        //return true;
        //}
        //public event EventHandler CanExecuteChanged;
        //public void Execute(object parameter){_action();}
    }


}
