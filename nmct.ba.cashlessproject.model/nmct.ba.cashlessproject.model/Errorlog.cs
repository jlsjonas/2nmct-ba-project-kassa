﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmct.ba.cashlessproject.model
{
    public class Errorlog : ObservableObject
    {
        private int _registerID;
        private DateTime _timestamp;
        private string _message;
        private string _stacktrace;

        public int RegisterId
        {
            get { return _registerID; }
            set
            {
                _registerID = value;
                OnPropertyChanged("RegisterID");
            }
        }

        public DateTime Timestamp
        {
            get { return _timestamp; }
            set
            {
                _timestamp = value;
                OnPropertyChanged("TimeStamp");
            }
        }

        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                OnPropertyChanged("Message");
            }
        }

        public string Stacktrace
        {
            get { return _stacktrace; }
            set
            {
                _stacktrace = value;
                OnPropertyChanged("Stacktrace");
            }
        }
    }
}
