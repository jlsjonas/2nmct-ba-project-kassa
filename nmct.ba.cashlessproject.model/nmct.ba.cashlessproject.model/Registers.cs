﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmct.ba.cashlessproject.model
{
    public class Registers:ObservableObject
    {
        private int _ID;
        private string _registerName;
        private string _device;
        private DateTime _purchaseDate;
        private DateTime _expiresDate;

        public int ID
        {
            get { return _ID; }
            set
            {
                _ID = value;
                OnPropertyChanged("ID");
            }
        }

        public string RegisterName
        {
            get { return _registerName; }
            set
            {
                _registerName = value;
                OnPropertyChanged("RegisterName");
            }
        }

        public string Device
        {
            get { return _device; }
            set
            {
                _device = value;
                OnPropertyChanged("Device");
            }
        }

        public DateTime PurchaseDate
        {
            get { return _purchaseDate; }
            set
            {
                _purchaseDate = value;
                OnPropertyChanged("PurchaseDate");
            }
        }

        public DateTime ExpiresDate
        {
            get { return _expiresDate; }
            set
            {
                _expiresDate = value;
                OnPropertyChanged("ExpiresDate");
            }
        }
    }
}
