﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nmct.ba.cashlessproject.model
{
    public class Sales : ObservableObject
    {
        private int _ID;
        private DateTime _timestamp;
        private Customers _customer;
        private Registers _register;
        private Products _Product;
        private double _amount;
        private double _totalPrice;


        public int ID
        {
            get { return _ID; }
            set { _ID = value; OnPropertyChanged("ID"); }
        }

        public DateTime Timestamp
        {
            get { return _timestamp; }
            set { _timestamp = value; OnPropertyChanged("Timestamp"); }
        }

        public Customers Customer
        {
            get { return _customer; }
            set { _customer = value; OnPropertyChanged("Customer"); }
        }

        public Registers Register
        {
            get { return _register; }
            set { _register = value; OnPropertyChanged("Register"); }
        }

        public Products Product
        {
            get { return _Product; }
            set { _Product = value; OnPropertyChanged("Product"); }
        }

        public double Amount
        {
            get { return _amount; }
            set { _amount = value; OnPropertyChanged("Amount"); }
        }

        public double TotalPrice
        {
            get { return _totalPrice; }
            set { _totalPrice = value; OnPropertyChanged("TotalPrice"); }
        }
    }
}
